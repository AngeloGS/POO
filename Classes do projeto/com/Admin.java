package com;

import com.ListaQuestoes.ListQuestoes;
import com.ListaQuestoes.Questao.Aberta;
import com.ListaQuestoes.Questao.Objetiva;
import com.ListaQuestoes.Questao.Questao;
import com.ListaQuestoes.Questao.VouF;
import com.ListaQuestoes.Assuntos;

import java.util.List;

public class Admin {
    private ListQuestoes listQuestoes;
    private Assuntos assuntos;

    public Admin(){ //Construtor
        listQuestoes = new ListQuestoes(); //estanceia o objeto ListQuestoes
        assuntos = new Assuntos(); //estancia o objeto assuntos
    }

    public void addQuestao(String pergunta, String assunto, String resposta){ //adiciona questoes abertas.
        Questao questao = new Aberta(pergunta, assunto, resposta);
        assuntos.addQuestao(assunto, questao);
        listQuestoes.add(questao);
    }

    public void addQuestao(String pergunta, String assunto, String[] alternativas, int[] corretas){ //adiciona questoes objetivas
        Questao questao = new Objetiva(pergunta, assunto, alternativas, corretas);
        assuntos.addQuestao(assunto, questao);
        listQuestoes.add(questao);
    }

    public void addQuestao(String pergunta, String assunto, boolean resposta){ //adiciona questoes de V ou F
        Questao questao = new VouF(pergunta, assunto, resposta);
        assuntos.addQuestao(assunto, questao);
        listQuestoes.add(questao);
    }

    public void addQuestao(Questao questao){ //adiciona objetos questoes.
        assuntos.addQuestao(questao.getAssunto(),questao);
        listQuestoes.add(questao);
    }

    public void addAssunto(String assunto){ //Adiciona assunto
        this.assuntos.addAssunto(assunto);
    }

    public List getNomeAssuntosList(){ //pega uma lista com nomes de todos os assuntos
        return assuntos.getNomeAssuntos();
    }

    public List getQuestoesPorAssunto(String assunto){ //pega a lista de questoes de algum assunto;
        return this.assuntos.getQuestoesAssunto(assunto);
    }

    public Questao getQuestao(int ID){ //pega uma questao, procura pelo o ID passado como parametro e retorna o objeto Questao.
        return listQuestoes.getQuestao(ID);
    }

    public List<Questao> search(String keyword){ //pesquisa uma por questoes que possuam a keyword.
        return listQuestoes.getQuestao(keyword);
    }

    public List<Questao> search(String keyword,String assunto){ //pesquisa por questoes de algum assunto que possuam a keyword
        return assuntos.getQuestao(keyword, assunto);
    }

    public int getNumQuestoes(){ //pegar numero de questoes cadastradas.
        return listQuestoes.getSizeList();
    }

    public boolean removerQuestao(String assunto, int ID){ //remove uma quetao de algum assunto olhando pelo ID.
        return assuntos.removeQuestao(assunto,ID);
    }

    public boolean removerQuestao(int ID){ //remove uma questao pelo ID.
        return listQuestoes.removeQuestao(ID);
    }
}