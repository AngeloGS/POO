package com.simulado.interfacegrafica;

import com.simulado.Questao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Assuntos {

    private HashMap<String, Assunto> osAssuntos;
    private List<String> assuntos;
    private List <String> assuntosDeletados;

    public Assuntos(){

        osAssuntos = new HashMap<>();
        assuntosDeletados = new ArrayList<>();
        assuntos = new ArrayList<>();

    }

    /*
    Esse metodo ao adicionar a questao, checa se o assunto existe no HashMap
    Se sim, adiciona no respectivo assunto, tudo usando a string com o nome do assunto como chave
    Se nao, cria um novo assunto e insere a questao no hashmap com o identificador sendo o nome do assunto criado
     */
    public boolean addQuestao(String assunto,Questao questao){

        if(osAssuntos.containsKey(assunto)){
            osAssuntos.get(assunto).addQuestao(questao);
            return true;
        }
        else{
            osAssuntos.put(assunto, new Assunto(assunto));
        }
        return false;
    }

    public List<String> getAssuntosDeletados() {
        return assuntosDeletados;
    }

    public boolean removeQuestao(String assunto, int id){
        if(osAssuntos.containsKey(assunto)){
            osAssuntos.get(assunto).removeQuestao(id);
            return true;
        }
        return false;
    }

    //Esse metodo simplesmente retorna um assunto dado seu nome

    public Assunto getAssunto(String nomeAssunto){
        return osAssuntos.get(nomeAssunto);
    }

    //Dado o nome de um assunto, para criar sem ligacao com questoes, o metodo adiciona ele ao hashmap, caso o assunto ja exista ele retorna false

    public boolean addAssunto(String nomeAssunto){
        if(!osAssuntos.containsKey(nomeAssunto)) {
            osAssuntos.put(nomeAssunto, new Assunto(nomeAssunto));
            assuntos.add(nomeAssunto);
            return true;
        }
        return false;
    }



    //Esse metodo simplesmente remove o assunto cujo nome foi passado. Caso nao exista o assunto, retorna falso

    public boolean removeAssunto(String nomeAssunto){
        if(osAssuntos.containsKey(nomeAssunto)){

            osAssuntos.remove(nomeAssunto);
            assuntos.remove(nomeAssunto);
            assuntosDeletados.add(nomeAssunto);
            return true;
        }
        return false;
    }

    //retorna lista com o nome de todos os assuntos cadastrados
    public List<String> getNomeAssuntos(){
        return assuntos;
    }
    public List<Questao> getQuestoesAssunto(String nomeAssunto){
        return getAssunto(nomeAssunto).getListaDeQuestoes();
    }

    public List<Questao> getQuestao(String keyword, String assunto){

        List<Questao> listaBusca = new ArrayList<>();

        for (Questao q: getQuestoesAssunto(assunto)) {
            if(q.getPergunta().replace(" ","").toLowerCase().contains(keyword)){

                listaBusca.add(q);

            }
        }
        return listaBusca;

    }

}
