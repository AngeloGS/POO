import java.util.ArrayList;
import java.util.List;

public class ListQuestoes {
    List<Questao> questoes = new ArrayList<Questao>();

    public void add(Questao questao){
        questoes.add(questao);
    }

    public Questao getQuestao(long ID){
        for(int i=0; i<questoes.size() ; i++){
            if(questoes.get(i).getID()==ID){
                return questoes.get(i);
            }
        }
        return null;
    }

    public List<Questao> getQuestao(String keyWord){
        List<Questao> resultadoPesquisa = new ArrayList<Questao>();
        for(int i=0; i<questoes.size() ; i++){
            if(questoes.get(i).getPergunta().replace(" ","").toLowerCase().contains(keyWord.toLowerCase().replace(" ",""))){
                resultadoPesquisa.add(questoes.get(i));
            }
        }
        return resultadoPesquisa;
    }

    public boolean excluirQuestao(long ID) {
        for(int i=0; i<questoes.size() ; i++){
            if(questoes.get(i).getID()==ID){
                questoes.remove(i);
            }
        }
        return false;
    }
}