public class Objetiva extends Questao{
    private String[] alternativas;
    private int[] corretas;

    public Objetiva(String pergunta, String assunto, String[] alternativas, int[] corretas) {
        //É passado para superclasse os parametros padroes de todas as questoes
        super(pergunta,assunto,"Objetiva");
        this.alternativas=alternativas;
        this.corretas=corretas;
    }

    public boolean setAlternativa(int posicao, String alternativa){
        if(posicao<alternativas.length && posicao>=0) {
            alternativas[posicao] = alternativa;
            return true;
        }else{
            return false;
        }
    }

    public String getAlternativa(int posicao){
        if(posicao<alternativas.length && posicao>=0) {
            return alternativas[posicao];
        }else{
            return "";
        }
    }

    public int getPosicao(String alternativa){
        for(int i=0; i<alternativas.length; i++){
            if(alternativas[i]==alternativa){
                return i;
            }
        }
        return -1;
    }

    public String getAlternativaAleatoria(){
        int num= (int)(Math.random()*100)%(alternativas.length);
        return getAlternativa(num);
    }

    public boolean verificaAlternativa(int posicao){
        if(posicao<alternativas.length && posicao>=0){
            for(int i=0; i<corretas.length; i++){
                if(corretas[i]==posicao){
                    return true;
                }
            }
            return false;
        }else{
            return false;
        }
    }

    public boolean verificaAlternativa(String alternativa){
        for(int i=0; i<alternativas.length; i++){
            if(alternativas[i]==alternativa){
                return true;
            }
        }
        return false;
    }

    public String[] getAlternativasOrdemAleatoria(){
        String[] alterativas= new String[this.alternativas.length];
        String alt;
        boolean repetir=false;
        for(int i=0; i<this.alternativas.length; i++){
            do {
                repetir=false;
                alt = getAlternativaAleatoria();
                for(int j=0; j<alterativas.length; j++){
                    if(alt.equals(alterativas[j])){
                        repetir=true;
                        break;
                    }
                }
            }while(repetir);
            alterativas[i]=alt;
        }
        return alterativas;
    }

}
