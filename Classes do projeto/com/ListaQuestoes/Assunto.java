package com.ListasQuestoes;


import com.ListasQuestoes.Questao.Questao;
import java.util.ArrayList;
import java.util.List;

public class Assunto {

    private String nome;
    private List<Questao> listaDeQuestoes;

    public List<Questao> getListaDeQuestoes() {
        return listaDeQuestoes;
    }

    public boolean addQuestao(Questao questao){
        listaDeQuestoes.add(questao);
        return true;
    }

    public boolean removeQuestao(int id){
        for (Questao q: listaDeQuestoes) {
            if(q.getID() == id){
                listaDeQuestoes.remove(q);
                return true;
            }
        }
        return false;
    }

    public Assunto(String nome){

        this.nome = nome;
        this.listaDeQuestoes = new ArrayList<>();

    }

}
