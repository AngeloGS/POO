package com.Telas.TelaBusca;


import com.Admin;
import com.ListaQuestoes.Questao.Questao;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class TelaBusca extends JFrame {
    //object admin, recebido como parametro{
        private Admin admin;
    //}-----------------------------------

    //Objeto Painel Main{
    private JPanel main;

        //Objetos do Painel areaPesquisa{
        private JPanel areaPesquisa;
            //Objetos do Painel esquerdo{
            JPanel esquerdo;
                private JLabel filtro;
                private JComboBox selecFiltro;
            //}----------------------------
            //Objetos do Painel direito{
            JPanel direito;
                private JTextField barraPesquisa;
                private JLabel pesquisar;
            //}---------------------------
        //}-------------------------------

        //Objetos do Painel table{
        private JPanel table;
            //Objetos do Painel cabeçalho{
            JPanel cabecalho;
                JLabel ID;
                JLabel assunto;
                JLabel tipo;
                JLabel pergunta;
            //----------------------------
            //Objetos do Painel lista{
            JPanel lista;
                private JList<JPanel> listaQuestoes;
                private JScrollPane scroll;
            //}---------------------------------
        //}--------------------------------

        //Objetos do Painel botton{
        private JPanel botton;
            JLabel sair;
            JLabel resposta;
            JLabel excluir;
            JLabel editar;
        //}------------------------------

    //}--------------------------------------


    public TelaBusca(Admin admin){
        //this.admin=admin;
        main = new JPanel(new BorderLayout());
        elementosPainelMain();
        add(main);
        //----------------------
        setSize(800,800);
        setVisible(true);
    }

    public void elementosPainelMain(){
        areaPesquisa = new JPanel(new GridLayout(1,2));
        elementosPainelAreaPesquisa();
        main.add(areaPesquisa, BorderLayout.NORTH);

        table = new JPanel(new BorderLayout());
        elementosPainelTable();
        main.add(table, BorderLayout.CENTER);

        botton = new JPanel(new GridLayout(1,4));
        elementosPainelBotton();
        main.add(botton, BorderLayout.SOUTH);
    }

    public void elementosPainelAreaPesquisa(){
        formatL1(areaPesquisa);
        //-----------------------------------------------
        esquerdo = new JPanel(new FlowLayout());
        elementosPainelesquerdo();
        //-----------------------------------
        direito = new JPanel(new FlowLayout());
        elementosPaineldireito();
        //---------------------------------------
        areaPesquisa.add(esquerdo);
        areaPesquisa.add(direito);
    }

    public void elementosPainelesquerdo(){
        formatL1(esquerdo);
        filtro = new JLabel("Filtrar Assunto:  ");
        formatL3(filtro);
        selecFiltro = new JComboBox();

        selecFiltro.addItem("Sem Filtro");
        selecFiltro.setBackground(Color.WHITE);

        List assuntos = admin.getAssuntosList();
        for(int i=0; i<assuntos.size(); i++) {
            selecFiltro.addItem(assuntos.get(i));
        }

        esquerdo.add(filtro);
        esquerdo.add(selecFiltro);
    }

    public void elementosPaineldireito(){
        formatL1(direito);
        barraPesquisa = new JTextField(15);
        pesquisar = new JLabel(new ImageIcon(getClass().getResource("Imagens/icons8-pesquisar_Black.png")));
        formatL1(pesquisar);
        pesquisar.addMouseListener(new BuscaMouseListener(0,pesquisar,"Imagens/icons8-pesquisar",this, admin));

        direito.add(barraPesquisa);
        direito.add(pesquisar);
    }

    public void elementosPainelTable(){
        cabecalho = new JPanel(new GridLayout(1,4));
        elementosPainelCabecalho();
        lista = new JPanel(new FlowLayout());
        formatL2(lista);
        elementosPainelLista();
        table.add(cabecalho, BorderLayout.NORTH);
        table.add(lista, BorderLayout.CENTER);

    }

    public void elementosPainelCabecalho(){
        ID = new JLabel("                           ID");
        formatL2(ID);
        assunto = new JLabel("                       Assunto");
        formatL2(assunto);
        tipo = new JLabel("                    Tipo");
        formatL2(tipo);
        pergunta = new JLabel("                 pergunta");
        formatL2(pergunta);
        cabecalho.add(ID);
        cabecalho.add(assunto);
        cabecalho.add(tipo);
        cabecalho.add(pergunta);
    }

    public void elementosPainelLista(){
        scroll = new JScrollPane();
        scroll.setViewportView(listaQuestoes);
        lista.add(scroll);
    }

    public void elementosPainelBotton(){
        sair = new JLabel(new ImageIcon(getClass().getResource("Imagens/icons8-sair_Black.png")));
        sair.addMouseListener(new BuscaMouseListener(1, sair,"Imagens/icons8-sair",this, admin));

        resposta = new JLabel(new ImageIcon(getClass().getResource("Imagens/icons8-informações_Black.png")));
        resposta.addMouseListener(new BuscaMouseListener(2,resposta,"Imagens/icons8-informações",this, admin));

        excluir = new JLabel(new ImageIcon(getClass().getResource("Imagens/icons8-excluir_Black.png")));
        excluir.addMouseListener(new BuscaMouseListener(3,excluir,"Imagens/icons8-excluir",this, admin));

        editar = new JLabel(new ImageIcon(getClass().getResource("Imagens/icons8-editar_Black.png")));
        editar.addMouseListener(new BuscaMouseListener(4,editar,"Imagens/icons8-editar",this, admin));

        botton.add(sair);
        botton.add(resposta);
        botton.add(excluir);
        botton.add(editar);
    }

    public void formatL1(JLabel label){
        label.setOpaque(true);
        label.setBackground(new Color(255, 255, 255));
    }

    public void formatL2(JLabel label){
        label.setOpaque(true);
        Color azulEscuro = new Color(0,53,101);
        label.setBackground(azulEscuro);
        label.setForeground(Color.WHITE);
        label.setBorder(BorderFactory.createBevelBorder(1,azulEscuro,azulEscuro,azulEscuro,Color.WHITE));
    }

    public void formatL3(JLabel label){
        label.setOpaque(true);
        Color azulEscuro = new Color(0,53,101);
        label.setBackground(azulEscuro);
        label.setForeground(Color.WHITE);
    }

    public void formatL1(JPanel painel){
        painel.setBackground(new Color(0,53,105));
    }

    public void formatL2(JPanel painel){
        painel.setBackground(Color.WHITE);
        painel.setBorder(BorderFactory.createBevelBorder(1));
    }

    public void pesquisarAction(){
       String textPesquisa = barraPesquisa.getText();
       List<Questao> resultados;
       if(selecFiltro.getSelectedItem()=="Sem Filtro"){
           resultados= admin.search(textPesquisa);
       }else{
           resultados = admin.search(textPesquisa, selecFiltro.getSelectedItem() + "");
       }

       listaQuestoes= new JList<JPanel>();

       for(int i=0; i<resultados.size() ;i++){
           JPanel painel = new JPanel(new GridLayout(1,4));
           painel.add(new JLabel(resultados.get(i).getID() + ""));
           painel.add(new JLabel(resultados.get(i).getAssunto()));
           painel.add(new JLabel(resultados.get(i).getTipo()));
           painel.add(new JLabel(resultados.get(i).getPergunta()));
           listaQuestoes.add(painel);
       }
    }

    public static void main(String[] args) {
        TelaBusca myframe = new TelaBusca();
    }
}
