package com.Telas.TelaBusca;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class BuscaMouseListener implements MouseListener {

    private JLabel label;
    private String dir;
    private int ID;
    private TelaBusca telaBusca;
    private Admin admin;

    public BuscaMouseListener(int ID, JLabel label, String dir, TelaBusca telaBusca, Admin admin){
        this.ID=ID;
        this.label = label;
        this.dir = dir;
        this.telaBusca=telaBusca;
        this.admin=admin;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        label.setBackground(new Color(155, 0, 0));
        if(ID==0){
         telaBusca.pesquisarAction();
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
        label.setIcon(new ImageIcon(getClass().getResource(dir + "_White.png")));
        label.setOpaque(true);
        label.setBackground(new Color(177, 0, 0));
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
        label.setIcon(new ImageIcon(getClass().getResource(dir + "_Black.png")));
        if(ID==0) {
            label.setBackground(Color.WHITE);
        }else{
            label.setOpaque(false);
        }
    }
}
