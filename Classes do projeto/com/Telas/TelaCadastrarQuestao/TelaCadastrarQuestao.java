package com.Telas.TelaCadastrarQuestao;

import com.Admin;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class TelaCadastrarQuestao {
    //Objeto nescessario para execução:
    Admin admin;
    //fim----------------------

    GridBagConstraints gbc = new GridBagConstraints();

    JFrame cadastrarQuestao= new JFrame("Cadastrar questão");

    //LIST OF IMAGES
    ArrayList listaDeImagens= new ArrayList<ImageIcon>();

    //caso tenha dúvidas, essa identação deixa bem claro quem é adicionada em quem
    //painel principal no Border Layout=Center da janela
    JPanel main= new JPanel(new BorderLayout());
        //painel com as ações subsequentes janelas seguintes
        JPanel PainelPrincipal= new JPanel(new CardLayout());
            //==================================================================================\\
            JPanel painelSelecTipo= new JPanel(new GridBagLayout());
                JPanel painelcomCombobox= new JPanel(new GridBagLayout());
                    JComboBox comboxTipoDeQuestoes= new JComboBox();
                    JComboBox comboxListaDeAssuntos= new JComboBox();
            //===================================================================================\\

            //==================================================================================\\
            JPanel painelPerguntaAberta= new JPanel(new GridBagLayout());

                JTextField areaDePergunta=new JTextField("Digite a pergunta aqui ");
                JTextField areaDeResposta=new JTextField("Digite a resposta aqui ");
            //===================================================================================\\
            JPanel painelPerguntaMultipla= new JPanel(new BorderLayout());
                JPanel paineldePerguntaGeral=new JPanel(new FlowLayout());
                JPanel painelRespostaMultipla= new JPanel(new GridBagLayout());

                JTextField areaDePergunta1=new JTextField("Digite a pergunta aqui ");

                JRadioButton[] alternativaBotao= new JRadioButton[]{
                        new JRadioButton("(A)"),
                        new JRadioButton("(B)"),
                        new JRadioButton("(C)"),
                        new JRadioButton("(D)"),
                        new JRadioButton("(E)"),
                        new JRadioButton("(F)")};

                JTextField alternativaTexto[]=new JTextField[]{
                        new JTextField(),
                        new JTextField(),
                        new JTextField(),
                        new JTextField(),
                        new JTextField(),
                        new JTextField()};

            //====================================================================================\\
            JPanel painelVouF= new JPanel(new GridBagLayout());
                JPanel paineldePerguntaGeral1=new JPanel(new GridBagLayout());
                    JTextField areaDePergunta2=new JTextField("Digite a pergunta aqui ");
                JPanel paineldeRespostaVouF=new JPanel(new GridLayout(1,2));
                    JComboBox comboBoxVouF= new JComboBox();

            //=====================================================================================\\

            JPanel painelConfirmar=new JPanel();
                JPanel painelConfirmarComMensagem=new JPanel();
                    JLabel labelComTipoDeQuestao=new JLabel();

            //=====================================================================================\\
            JPanel painelConcluido= new JPanel(new GridBagLayout());
                JPanel painelComMensagens= new JPanel(new GridBagLayout());
                    JLabel mensagem= new JLabel("Questão cadastrada com sucesso!!");


    //===================================================================================================\
    JPanel PainelInferior= new JPanel(new GridLayout(1,3));
        JLabel botaoavanca;
        JLabel botaovolta;
    //=====================================================================================================\\


    //ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ//

    public TelaCadastrarQuestao(Admin admin){
        this.admin = admin;

        listaDeImagens.add(new ImageIcon("src/com/Telas/Imagens/redo.png"));
        listaDeImagens.add(new ImageIcon("src/com/Telas/Imagens/undo.png"));
        listaDeImagens.add(new ImageIcon("src/com/Telas/Imagens/undoselected.png"));
        listaDeImagens.add(new ImageIcon("src/com/Telas/Imagens/redoselected.png"));
        listaDeImagens.add(new ImageIcon("src/com/Telas/Imagens/redopressed.png"));
        listaDeImagens.add(new ImageIcon("src/com/Telas/Imagens/undopressed.png"));

        botaoavanca =new JLabel((ImageIcon)listaDeImagens.get(0));
        botaovolta=new JLabel((ImageIcon)listaDeImagens.get(1));

        //COMBOBOX
       comboxListaDeAssuntos.addItem("Biologia");
       comboxListaDeAssuntos.addItem("Física");
       comboxListaDeAssuntos.addItem("Quimica");
       comboxListaDeAssuntos.addItem("Informática");

       comboxTipoDeQuestoes.addItem("Aberta");
       comboxTipoDeQuestoes.addItem("Objetiva");
       comboxTipoDeQuestoes.addItem("V ou F");


        //=======================================PAINEL SELECIONA TIPO DE QUESTAO======================================\\
        gbc.gridx=0; gbc.gridy=0;
        painelcomCombobox.add(new JLabel("Selecione um assunto: "),gbc);
        gbc.gridx=1; gbc.gridy=0;
        painelcomCombobox.add(comboxListaDeAssuntos,gbc);

        gbc.gridx=0; gbc.gridy=1;
        painelcomCombobox.add(new JLabel("Selecione um tipo de questão: "),gbc);
        gbc.gridx=1; gbc.gridy=1;
        painelcomCombobox.add(comboxTipoDeQuestoes,gbc);

        painelSelecTipo.add(painelcomCombobox);

        //=======================================FIM PAINEL SELECIONA TIPO DE QUESTAO===================================\\


        //========================================PAINEL DA PERGUNTA ABERTA=============================================//
        //espaçamento entre as celulas
        gbc.insets=new Insets(5,0,5,0);


        //-------------------------------------------------------------------------------//
        gbc.gridx=0;    gbc.gridy=2;                                                    //
         painelPerguntaAberta.add(new JLabel("Digite a pergunta: "),gbc);         //
        gbc.gridx=1;    gbc.gridy=2;                                                  //
         areaDePergunta.setPreferredSize(new Dimension(250,30));        //--AREA DE DIGITAR PERGUNTA
         areaDePergunta.addMouseListener(new MouseListernerTexto(areaDePergunta));  //
         painelPerguntaAberta.add(areaDePergunta,gbc);                             //
        //------------------------------------------------------------------------//
        //-------------------------------------------------------------------------------//
        gbc.gridx=0;    gbc.gridy=3;                                                    //
         painelPerguntaAberta.add(new JLabel("Digite a resposta: "),gbc);         //
        gbc.gridx=1;    gbc.gridy=3;                                                  //--AREA DE DIGITAR PERGUNTA
        areaDeResposta.addMouseListener(new MouseListernerTexto(areaDeResposta));    //
        areaDeResposta.setPreferredSize(new Dimension(250,30));         //
        //--------------------------------------------------------------------------//

        painelPerguntaAberta.setBackground(new Color(0x46487A));
        painelPerguntaAberta.add(areaDeResposta,gbc);
        //
        //                     FIM-PAINEL ABERTA
        //
        //=============================================================================================================\\

        //==========================================PAINEL PERGUNTA MULTIPLA=============================================\\


        //---------------------------------------------------------------------------//
        paineldePerguntaGeral.add(new JLabel("Digite a pergunta: "));           //
        paineldePerguntaGeral.add(areaDePergunta1);                                  //
        areaDePergunta1.setPreferredSize(new Dimension(250,30));        //------AREA DE DIGITAR PERGUNTA
        areaDePergunta1.addMouseListener(new MouseListernerTexto(areaDePergunta1)); //
        //-------------------------------------------------------------------------//

        //TRAVANDO O TAMANHO DE TODAS AS CAIXAS DE TEXTOS POIS O GRID BAG LAYOUT ALTERA

        for (int i = 0; i <alternativaTexto.length ; i++) {
            alternativaTexto[i].setPreferredSize(new Dimension(140,25));
        }

        //ALTERNATIVAS DAS QUESTOES--------------------------//
        //------------------------------------------------------------------------------//
        do {                                                                            //
            int x = 0, y = 1;                                                          //
                                                                                      //
            for (int i = 0; i < alternativaBotao.length; i++) {                      //
                gbc.gridx = x;                                                      //
                gbc.gridy = y;                                                     //
                painelRespostaMultipla.add(alternativaBotao[i], gbc);             //
                gbc.gridx = x + 1;                                               //
                gbc.gridy = y;                                                  //
                painelRespostaMultipla.add(alternativaTexto[i], gbc);          //------ESSE LAÇO COLOCA CADA BOTAO
                y++;                                                          //------EM SEU RESPECTIVO LUGAR
                if (y == 4) {                                                //-------NO PAINEL DAS ALTERNATIVAS
                    y = 1;                                                  //
                    x = 2;                                                 //
                }                                                         //
            }                                                            //
        }while(false);                                                  //
        //-------------------------------------------------------------//

        painelPerguntaMultipla.add(paineldePerguntaGeral,BorderLayout.NORTH);
        painelPerguntaMultipla.add(painelRespostaMultipla,BorderLayout.CENTER);


        //=================================================FIM PAINEL PERGUNTA MULTIPLA=================================\\

        //===============================================PAINEL PERGUNTA VERDADEIRA OU FALSA=============================\\


        //-----------------------------------------------------------------------------//
        gbc.gridx=0; gbc.gridy=0;                                                      //
        paineldePerguntaGeral1.add(new JLabel("Digite a pergunta:"),gbc);         //
        gbc.gridx=1; gbc.gridy=0;                                                     //
        paineldePerguntaGeral1.add(areaDePergunta2,gbc);                              // ------AREA DE DIGITAR PERGUNTA
                                                                                      //
        areaDePergunta2.setPreferredSize(new Dimension(250,30));         //
        areaDePergunta2.addMouseListener(new MouseListernerTexto(areaDePergunta2));  //
        //--------------------------------------------------------------------------//

        //------------------------------------//
        comboBoxVouF.addItem("Verdadeiro");  //
        comboBoxVouF.addItem("Falso");      //--SELECIONAR VERDADEIRO OU FALSO
        gbc.gridx=0; gbc.gridy=1;          //
        painelVouF.add(comboBoxVouF,gbc); //
        //-------------------------------//

        gbc.gridx=0; gbc.gridy=2;
        painelVouF.add(new JLabel("PS: lembre-se de adicionar 'É verdadeiro ou falso?'"),gbc);

        painelVouF.add(paineldePerguntaGeral1);
        //========================================FIM PAINEL VERDADEIRA OU FALSA========================================\\

        //========================================PAINEL CONFIRMA QUESTÃO===============================================\\


        //========================================FIM PAINEL CONFIRMA QUESTÃO===========================================\\


        //========================================PAINEL CADASTRADO COM SUCESSO=========================================\\

        mensagem.setBackground(new Color(0x00FF05));

        painelComMensagens.add(mensagem);

        painelConcluido.add(painelComMensagens);

        //========================================FIM PAINEL CADASTRADO COM SUCESO======================================\\


        //MAINFRAME---------------------------------------XXXXXXXXXXXXXXXXXXXXX-----------------------------------------\\
        PainelPrincipal.add(painelSelecTipo);
        PainelPrincipal.add(painelPerguntaAberta,"Aberta");
        PainelPrincipal.add(painelPerguntaMultipla,"Objetiva");
        PainelPrincipal.add(painelVouF,"V ou F");
        PainelPrincipal.add(painelConcluido,"sucesso");
        //------------------------------------------------XXXXXXXXXXXXXXXXXXXXX-----------------------------------------\\

        //[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[NÃO TOQUE AQUI]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]\\
                                                  main.add(PainelPrincipal);
        //[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[NÃO TOQUE AQUI]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]\\

        //========================================PAINEL INFERIOR COM OS BOTOES==========================================\\
        PainelInferior.add(botaovolta);
        PainelInferior.add(new JLabel(""));//JLABEL VAZIA PRA DEIXAR ESPAÇO ENTRE OS BOTOES
        PainelInferior.add(botaoavanca);
        PainelInferior.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));//criando espaçamento entre as celulas
        //------------------------------------------------------------------------------------------------/////
        botaoavanca.addMouseListener(new ImageListenet(PainelPrincipal,1,listaDeImagens,botaoavanca,comboxTipoDeQuestoes));
        botaovolta.addMouseListener(new ImageListenet(PainelPrincipal,-1,listaDeImagens,botaovolta,comboxTipoDeQuestoes));


        //=======================================FINAL DO PAINEL INFERIOR COM OS BOTOES===================================//

        cadastrarQuestao.add(main,BorderLayout.CENTER);
        cadastrarQuestao.add(PainelInferior,BorderLayout.SOUTH);
        cadastrarQuestao.setMinimumSize(new Dimension(400,300));
        cadastrarQuestao.setSize(400,300);

        cadastrarQuestao.setVisible(true);
        cadastrarQuestao.setResizable(true);
        cadastrarQuestao.setDefaultCloseOperation(cadastrarQuestao.EXIT_ON_CLOSE);
        
    }
}

class ImageListenet implements MouseListener {

    CardLayout umcard;
    JPanel apanel;
    int operacao;
    ImageIcon volta;
    ImageIcon avanca;
    ImageIcon voltaS;
    ImageIcon avancaS;
    JLabel esteLabel;

    ImageIcon voltaP;
    ImageIcon avancaP;

    JComboBox acombo;

    static int status=0;

    public ImageListenet(JPanel outpanel, int Toperacao,ArrayList listadeimagens, JLabel TesteLabel,JComboBox Tacombo){
       apanel= outpanel;
       umcard = (CardLayout)apanel.getLayout();
        operacao=Toperacao;
        volta=(ImageIcon)listadeimagens.get(0);
        avanca=(ImageIcon)listadeimagens.get(1);
        voltaS=(ImageIcon)listadeimagens.get(2);
        avancaS=(ImageIcon)listadeimagens.get(3);

        voltaP=(ImageIcon)listadeimagens.get(5);
        avancaP=(ImageIcon)listadeimagens.get(4);

        esteLabel=TesteLabel;

        acombo=Tacombo;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

        if(operacao==1){
            esteLabel.setIcon(avancaP);

            if(status==0) {
                umcard.show(apanel, (String) acombo.getSelectedItem());
            }else{
                if(status==1){
                    umcard.show(apanel,"sucesso");
                }
            }
            status++;
            if (status>2){
                status=2;
            }
        }else{
            esteLabel.setIcon(voltaP);
            status--;
            if(status<0){
                status=0;
            }

            if(status==0) {
                umcard.first(apanel);
            }
            else{
                umcard.show(apanel, (String) acombo.getSelectedItem());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if(operacao==1){
            esteLabel.setIcon(avancaS);
        }else{
            esteLabel.setIcon(voltaS);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if(operacao==1){
            esteLabel.setIcon(volta);
        }else{
            esteLabel.setIcon(avanca);
        }

    }
}

class MouseListernerTexto implements MouseListener{

    JTextField textField;

    public MouseListernerTexto(JTextField TtextField) {

        textField=TtextField;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(textField.getText().equals("Digite a pergunta aqui ") || textField.getText().equals("Digite a resposta aqui ")){
            textField.setText("");
        }
    }
    @Override
    public void mousePressed(MouseEvent e) { }
    @Override
    public void mouseReleased(MouseEvent e) {}
    @Override
    public void mouseEntered(MouseEvent e) { }
    @Override
    public void mouseExited(MouseEvent e){}
}
