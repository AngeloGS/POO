package com;


import com.Gerar.Gerar;
import com.Salvar.Salvar;
import com.Telas.TelaCadastrarQuestao.TelaCadastrarQuestao;
import com.Telas.TelaPrincipal.TelaPrincipal;

public class Primaria {

    private Admin admin;
    private Gerar objGerar;
    private Salvar objSalvar;
    private TelaCadastrarQuestao telaCadastrarQuestao;
    //private TelaGerarProva telaGerarProva; Susgestão de tela para gerar provas PDF,dock ou txt. não sei se é nescessario.
    private TelaPrincipal telaPrincipal;

    public Primaria(){
        admin = new Admin();
        objGerar = new Gerar(admin);
        objSalvar = new Salvar(admin);
        telaPrincipal = new TelaPrincipal(admin, objGerar, objSalvar);
    }

    public static void main(String[] args) {
        Primaria myProgam = new Primaria();
    }
}